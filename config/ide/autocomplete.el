(use-package company
  :ensure t
  :diminish
  :custom
  (company-begin-commands '(self-insert-command))
  (company-idle-delay .1)
  (company-minimum-prefix-length 2)
  (company-show-numbers t)
  (company-tooltip-align-annotations t)
  (company-show-numbers t)
  (global-company-mode t))
(use-package company-lsp
  :ensure t)
(require 'company-lsp)
(push 'company-lsp company-backends)
;; (use-package company-box
;;   :after company
;;   :diminish
;;   :hook (company-mode . company-box-mode))

(use-package lsp-mode
  :ensure t)
