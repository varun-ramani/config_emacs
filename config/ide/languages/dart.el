(use-package dart-mode
  :ensure t
  :defer t
  :config
  (setq dart-enable-analysis-server t))
